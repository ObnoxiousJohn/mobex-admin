/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "remittance_trail", schema="mobex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RemittanceTrail.findAll", query = "SELECT r FROM RemittanceTrail r"),
    @NamedQuery(name = "RemittanceTrail.findByPkRemittanceTrailId", query = "SELECT r FROM RemittanceTrail r WHERE r.pkRemittanceTrailId = :pkRemittanceTrailId"),
    @NamedQuery(name = "RemittanceTrail.findByServerTransactionDate", query = "SELECT r FROM RemittanceTrail r WHERE r.serverTransactionDate = :serverTransactionDate"),
    @NamedQuery(name = "RemittanceTrail.findByDeviceTransactionDate", query = "SELECT r FROM RemittanceTrail r WHERE r.deviceTransactionDate = :deviceTransactionDate")})
public class RemittanceTrail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_remittance_trail_id")
    private Long pkRemittanceTrailId;
    @Column(name = "server_transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date serverTransactionDate;
    @Column(name = "device_transaction_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deviceTransactionDate;
    @JoinColumn(name = "fk_remittance_id", referencedColumnName = "pk_remittance_id")
    @ManyToOne
    private Remittance fkRemittanceId;
    @JoinColumn(name = "fk_cfg_remittance_status_id", referencedColumnName = "pk_cfg_remittance_status_id")
    @ManyToOne
    private CfgRemittanceStatus fkCfgRemittanceStatusId;

    public RemittanceTrail() {
    }

    public RemittanceTrail(Long pkRemittanceTrailId) {
        this.pkRemittanceTrailId = pkRemittanceTrailId;
    }

    public Long getPkRemittanceTrailId() {
        return pkRemittanceTrailId;
    }

    public void setPkRemittanceTrailId(Long pkRemittanceTrailId) {
        this.pkRemittanceTrailId = pkRemittanceTrailId;
    }

    public Date getServerTransactionDate() {
        return serverTransactionDate;
    }

    public void setServerTransactionDate(Date serverTransactionDate) {
        this.serverTransactionDate = serverTransactionDate;
    }

    public Date getDeviceTransactionDate() {
        return deviceTransactionDate;
    }

    public void setDeviceTransactionDate(Date deviceTransactionDate) {
        this.deviceTransactionDate = deviceTransactionDate;
    }

    public Remittance getFkRemittanceId() {
        return fkRemittanceId;
    }

    public void setFkRemittanceId(Remittance fkRemittanceId) {
        this.fkRemittanceId = fkRemittanceId;
    }

    public CfgRemittanceStatus getFkCfgRemittanceStatusId() {
        return fkCfgRemittanceStatusId;
    }

    public void setFkCfgRemittanceStatusId(CfgRemittanceStatus fkCfgRemittanceStatusId) {
        this.fkCfgRemittanceStatusId = fkCfgRemittanceStatusId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkRemittanceTrailId != null ? pkRemittanceTrailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RemittanceTrail)) {
            return false;
        }
        RemittanceTrail other = (RemittanceTrail) object;
        if ((this.pkRemittanceTrailId == null && other.pkRemittanceTrailId != null) || (this.pkRemittanceTrailId != null && !this.pkRemittanceTrailId.equals(other.pkRemittanceTrailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.Entity.RemittanceTrail[ pkRemittanceTrailId=" + pkRemittanceTrailId + " ]";
    }
    
}
