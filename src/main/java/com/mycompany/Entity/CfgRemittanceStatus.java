/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author USER
 */
@Entity
@Table(name = "cfg_remittance_status", schema="mobex")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CfgRemittanceStatus.findAll", query = "SELECT c FROM CfgRemittanceStatus c"),
    @NamedQuery(name = "CfgRemittanceStatus.findByPkCfgRemittanceStatusId", query = "SELECT c FROM CfgRemittanceStatus c WHERE c.pkCfgRemittanceStatusId = :pkCfgRemittanceStatusId"),
    @NamedQuery(name = "CfgRemittanceStatus.findByCfgRemittanceStatusName", query = "SELECT c FROM CfgRemittanceStatus c WHERE c.cfgRemittanceStatusName = :cfgRemittanceStatusName")})
public class CfgRemittanceStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pk_cfg_remittance_status_id")
    private Integer pkCfgRemittanceStatusId;
    @Size(max = 50)
    @Column(name = "cfg_remittance_status_name")
    private String cfgRemittanceStatusName;
    @OneToMany(mappedBy = "fkCfgRemittanceStatusId")
    private Collection<RemittanceTrail> remittanceTrailCollection;
    @OneToMany(mappedBy = "fkCfgRemittanceStatusId")
    private Collection<Remittance> remittanceCollection;

    public CfgRemittanceStatus() {
    }

    public CfgRemittanceStatus(Integer pkCfgRemittanceStatusId) {
        this.pkCfgRemittanceStatusId = pkCfgRemittanceStatusId;
    }

    public Integer getPkCfgRemittanceStatusId() {
        return pkCfgRemittanceStatusId;
    }

    public void setPkCfgRemittanceStatusId(Integer pkCfgRemittanceStatusId) {
        this.pkCfgRemittanceStatusId = pkCfgRemittanceStatusId;
    }

    public String getCfgRemittanceStatusName() {
        return cfgRemittanceStatusName;
    }

    public void setCfgRemittanceStatusName(String cfgRemittanceStatusName) {
        this.cfgRemittanceStatusName = cfgRemittanceStatusName;
    }

    @XmlTransient
    public Collection<RemittanceTrail> getRemittanceTrailCollection() {
        return remittanceTrailCollection;
    }

    public void setRemittanceTrailCollection(Collection<RemittanceTrail> remittanceTrailCollection) {
        this.remittanceTrailCollection = remittanceTrailCollection;
    }

    @XmlTransient
    public Collection<Remittance> getRemittanceCollection() {
        return remittanceCollection;
    }

    public void setRemittanceCollection(Collection<Remittance> remittanceCollection) {
        this.remittanceCollection = remittanceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkCfgRemittanceStatusId != null ? pkCfgRemittanceStatusId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CfgRemittanceStatus)) {
            return false;
        }
        CfgRemittanceStatus other = (CfgRemittanceStatus) object;
        if ((this.pkCfgRemittanceStatusId == null && other.pkCfgRemittanceStatusId != null) || (this.pkCfgRemittanceStatusId != null && !this.pkCfgRemittanceStatusId.equals(other.pkCfgRemittanceStatusId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.Entity.CfgRemittanceStatus[ pkCfgRemittanceStatusId=" + pkCfgRemittanceStatusId + " ]";
    }
    
}
