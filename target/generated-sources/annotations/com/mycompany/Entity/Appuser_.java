package com.mycompany.Entity;

import com.mycompany.Entity.Device;
import com.mycompany.Entity.Remittance;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-01-20T14:38:06")
@StaticMetamodel(Appuser.class)
public class Appuser_ { 

    public static volatile SingularAttribute<Appuser, String> mpin;
    public static volatile SingularAttribute<Appuser, Integer> accountStatus;
    public static volatile CollectionAttribute<Appuser, Remittance> remittanceCollection;
    public static volatile SingularAttribute<Appuser, Integer> retries;
    public static volatile SingularAttribute<Appuser, Long> pkAppuserId;
    public static volatile CollectionAttribute<Appuser, Device> deviceCollection;
    public static volatile SingularAttribute<Appuser, Date> serverRegistrationDate;
    public static volatile SingularAttribute<Appuser, String> authid;

}