package com.mycompany.Entity;

import com.mycompany.Entity.Appuser;
import com.mycompany.Entity.CfgRemittanceStatus;
import com.mycompany.Entity.Device;
import com.mycompany.Entity.RemittanceTrail;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-01-20T14:38:06")
@StaticMetamodel(Remittance.class)
public class Remittance_ { 

    public static volatile SingularAttribute<Remittance, Date> latestDeviceTransactionDate;
    public static volatile SingularAttribute<Remittance, Appuser> fkAppuserId;
    public static volatile SingularAttribute<Remittance, Double> forexRate;
    public static volatile SingularAttribute<Remittance, String> currencyFrom;
    public static volatile SingularAttribute<Remittance, String> remittanceMonth;
    public static volatile SingularAttribute<Remittance, Date> latestServerTransactionDate;
    public static volatile SingularAttribute<Remittance, String> remittanceControlNumber;
    public static volatile SingularAttribute<Remittance, CfgRemittanceStatus> fkCfgRemittanceStatusId;
    public static volatile SingularAttribute<Remittance, String> currencyTo;
    public static volatile SingularAttribute<Remittance, Long> pkRemittanceId;
    public static volatile SingularAttribute<Remittance, Double> amountFrom;
    public static volatile SingularAttribute<Remittance, Device> fkDeviceId;
    public static volatile SingularAttribute<Remittance, Double> remittanceFee;
    public static volatile CollectionAttribute<Remittance, RemittanceTrail> remittanceTrailCollection;

}