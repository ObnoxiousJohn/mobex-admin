package com.mycompany.Entity;

import com.mycompany.Entity.Remittance;
import com.mycompany.Entity.RemittanceTrail;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.1.v20130918-rNA", date="2014-01-20T14:38:06")
@StaticMetamodel(CfgRemittanceStatus.class)
public class CfgRemittanceStatus_ { 

    public static volatile CollectionAttribute<CfgRemittanceStatus, Remittance> remittanceCollection;
    public static volatile SingularAttribute<CfgRemittanceStatus, Integer> pkCfgRemittanceStatusId;
    public static volatile SingularAttribute<CfgRemittanceStatus, String> cfgRemittanceStatusName;
    public static volatile CollectionAttribute<CfgRemittanceStatus, RemittanceTrail> remittanceTrailCollection;

}